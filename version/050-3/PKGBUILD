# Obarun        : 66 init/supervisor
# Maintainer    : Eric Vidal <eric@obarun.org>
# Maintainer    : Jean-Michel T.Dydak <jean-michel@obarun.org>
# Obarun PkgSrc : https://framagit.org/pkg/obextra/dracut/
#----------------
# Maintainer    : Giancarlo Razzolini <grazzolini@archlinux.org>
# Arch PkgSrc   : https://www.archlinux.org/packages/extra/x86_64/dracut/
#----------------
# Website       : https://dracut.wiki.kernel.org/index.php/Main_Page/
#----------------------------------------------------------------------------
#--DESCRIPTION---------------------------------------------------------------

pkgname=dracut

pkgdesc="An event driven initramfs infrastructure"

pkgver=050
pkgrel=3

url="https://github.com/dracutdevs/dracut"

source=("${pkgname}-${pkgver}.tar.gz::${url}/archive/${pkgver}.tar.gz"
        'dracut-install'
        'dracut-remove'
        '90-dracut-install.hook'
        '60-dracut-remove.hook')

#--INSTALL CONFIGURATION-----------------------------------------------------

arch=(x86_64)

backup=(
    'etc/dracut.conf')

depends=(
    'bash'
    'coreutils'
    'cpio'
    'filesystem'
    'findutils'
    'grep'
    'gzip'
    'kmod'
    'procps-ng'
    'sed'
    'util-linux'
    'xz')

provides=(
    'initramfs')

conflicts=(
    'dracut-git'
    'mkinitcpio'
    'mkinitcpio-busybox')

#--BUILD CONFIGURATION-------------------------------------------------------

makedepends=(
    'asciidoc'
    'bash-completion')

#--BUILD CONTROL-------------------------------------------------------------

_path=(
    --prefix=/usr
    --sysconfdir=/etc
)

#--BUILD---------------------------------------------------------------------

build() {
    cd "$pkgname-$pkgver"

    ./configure "${_path[@]}"
    make
}

#--PACKAGE-------------------------------------------------------------------

package() {
    cd "$pkgname-$pkgver"

    DESTDIR="${pkgdir}" make install

    ## We don't need the systemd stuff.
    rm -rf ${pkgdir}/usr/lib/dracut/modules.d/*systemd*
    rm  -f ${pkgdir}/usr/share/man/man8/*.service.*
    rm -rf ${pkgdir}/usr/lib/kernel

    install -Dm644 "${srcdir}/90-dracut-install.hook" "${pkgdir}/usr/share/libalpm/hooks/90-dracut-install.hook"
    install -Dm644 "${srcdir}/60-dracut-remove.hook"  "${pkgdir}/usr/share/libalpm/hooks/60-dracut-remove.hook"
    install -Dm755 "${srcdir}/dracut-install"         "${pkgdir}/usr/share/libalpm/scripts/dracut-install"
    install -Dm755 "${srcdir}/dracut-remove"          "${pkgdir}/usr/share/libalpm/scripts/dracut-remove"
}

#--SECURITY AND LICENCE------------------------------------------------------

sha512sums=('4b5b50eb241594afee927a8a24097d30353674e2ff474f0abeab8f2a7dbc1c6a089f79825fd1bae6ea01c71fefbd76c335cea00ecb593883f35c350328772c64'
            'ecc9ac4fdb4fff49f7639e68f499ca5735c0aa1ab385ed124f857c7201228016ead6fb6ad8edf828dcb45795387701ac1aada408298a13420a060b43686519df'
            '0aeded52d05934a05e00560d79d1425933b9178206fd78685551228745e35744a15f4f6a4c1d1269a28a9039fcfb1ec493c9453df5211d560934c347815d2ef7'
            'c06426c649d7c80c65a52b60499923029c4729c6a458d7eedbd700f2469e812400ea950dcc873f8ae551a47961c1a242ad8ac4435e32f36a0e6195dedec4508b'
            'd7766084b3dee88cca43393964660729d94cbcf0743584252422ce5b280f1f1968e0f94a8d7147cac2db62e1c8d28ea8972b6a67151aa42f6b710f176ce75734')

license=('GPL')
